{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}

module GitIndex (
    GitIndex (..),
    GitIndexEntry (..),
    loadIndexFile
) where

import qualified Codec.Compression.Zlib as Zlib
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as VU

import Control.Monad
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Debug.Trace
import System.Directory
import System.FilePath
import System.IO.Posix.MMap.Lazy

import GitObject
import GitUtil

data GitIndex = GitIndex {
    indexVersion        :: Int32,
    indexEntryCount     :: Int32,
    indexEntries        :: V.Vector GitIndexEntry
}

data GitIndexEntry = GitIndexEntry {
    -- Timestamps
    indexEntryCtime     :: Int32,
    indexEntryCtimeNsec :: Int32,
    indexEntryMtime     :: Int32,
    indexEntryMtimeNsec :: Int32,
    
    -- Stat information
    indexEntryDev       :: Int32,
    indexEntryInode    :: Int32,
    indexEntryMode      :: Int32,
    indexEntryUid       :: Int32,
    indexEntryGid       :: Int32,
    indexEntrySize      :: Int32,
    
    -- Git-specific fields
    indexEntryHash      :: GitHash,
    indexEntryFlags     :: Int32,
    indexEntryName      :: BS.ByteString
}

getIndex :: Get GitIndex
getIndex = do
    let getInt32 = liftM fromIntegral getWord32be
    
    -- Index signature
    signature <- getInt32
    when (signature /= 0x44495243) $ -- "DIRC"
        fail "Invalid git index signature"
    
    -- Index version
    version <- getInt32
    when (version /= 2) $
        fail "Invalid git index version"
    
    -- Index entry count
    entryCount <- getInt32
    
    -- Index entries
    entries <- V.replicateM (fromIntegral entryCount) getIndexEntry
    
    return $ GitIndex version entryCount entries

getIndexEntry :: Get GitIndexEntry
getIndexEntry = do
    let getInt32 = liftM fromIntegral getWord32be
    startPos    <- bytesRead
    
    -- Timestamps
    ctime       <- getInt32
    ctimeNsec   <- getInt32
    mtime       <- getInt32
    mtimeNsec   <- getInt32
    
    -- Stat information 
    dev         <- getInt32
    inode       <- getInt32
    mode        <- getInt32
    uid         <- getInt32
    gid         <- getInt32
    size        <- getInt32
    
    -- Git-specific fields
    hash        <- getByteString 20
    entryFlags  <- getWord16be
    let flags   = fromIntegral $ entryFlags `shiftR` 12
    let nameLen = fromIntegral $ entryFlags .&. 0xfff
    name        <- getByteString nameLen
    
    -- Padding
    endPos      <- bytesRead
    skip        (fromIntegral $ 8 - ((endPos-startPos) `mod` 8))
    
    return $ GitIndexEntry ctime ctimeNsec mtime mtimeNsec dev inode mode uid gid size hash flags name

loadIndexFile :: FilePath -> IO GitIndex
loadIndexFile filename = do
    contents <- unsafeMMapFile filename
    return $ runGet getIndex contents
