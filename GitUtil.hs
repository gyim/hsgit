{-# LANGUAGE BangPatterns #-}

module GitUtil where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.ByteString.UTF8 as BSU
import qualified Data.Vector as V

import Control.Monad
import Data.Bits
import GHC.Word

-- Shortcut functions
toStrict bs = BS.concat (B.toChunks bs)
toLazy bs = B.fromChunks [bs]
toByteString = BSU.fromString
toString = BSU.toString
int = fromIntegral

{-# RULES "toByteString/toString" forall s. toByteString (toString s) = s #-}
{-# RULES "toString/toByteString" forall s. toString (toByteString s) = s #-}

-- Parsing
splitAtChar c bs =
    case (B.findIndex (==c) bs) of
        Just i  -> (B.take i bs, B.drop (i+1) bs)
        Nothing -> (bs, B.empty)

-- Converting hexa code
toHex :: BS.ByteString -> BS.ByteString
toHex bs = BS.unfoldr step (0,True) where
    l = BS.length bs
    step (!i,!isUpperBits)
        | i >= l        = Nothing
        | isUpperBits   = Just (toHexChar (c `shiftR` 4), (i,False))
        | otherwise     = Just (toHexChar (c .&. 0x0f), (i+1,True))
        where
            c = BS.index bs i
            toHexChar c = if c < 10 then c+48 else (c-10)+97

fromHex :: BS.ByteString -> BS.ByteString
fromHex bs = BS.unfoldr step 0 where
    l = (BS.length bs) `div` 2
    step i = if i >= l then Nothing else Just (c,i+1) where
        c1 = fromHexChar $ BS.index bs (i*2)
        c2 = fromHexChar $ BS.index bs (i*2+1)
        c  = (c1 `shiftL` 4) .|. c2
    fromHexChar c
        | c >= 48 && c <= 57  = c-48    -- 0-9
        | c >= 97 && c <= 102 = c-97+10 -- a-f
        | c >= 65 && c <= 70  = c-65+10 -- A-F
        | otherwise = 0

-- NOTE: These rules true only true if the argument of fromHex is a
-- "real" hex string
{-# RULES "toHex/fromHex" forall bs. toHex (fromHex bs) = bs #-}
{-# RULES "fromHex/toHex" forall bs. fromHex (toHex bs) = bs #-}

-- Monad utilities
andM :: Monad m => m Bool -> m Bool -> m Bool
andM m1 m2 = do
    r <- m1
    if r then m2 else return False

vecWhileM :: Monad m => V.Vector a -> (a -> m Bool) -> m Bool
vecWhileM v f = V.foldM' step True v where
    step True val = f val
    step False _ = return False

vecFindFirstM :: Monad m => V.Vector a -> (a -> m (Maybe b)) -> m (Maybe b)
vecFindFirstM v f = V.foldM' step Nothing v where
    step Nothing val = f val
    step result@(Just val) _ = return result
