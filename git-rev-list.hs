import qualified Data.Vector as V
import Control.Monad
import System.Environment
import System.Exit

import GitObject
import GitRepo
import GitUtil

printCommit commitId commit s = do
    putStrLn $ toString (toHex commitId)
    return (Just s)

main = do
    -- Parse arguments
    args <- getArgs
    when ((length args) < 1) $ do
        putStrLn "Usage: git-show <hash>"
        exitFailure

    let hash = fromHex (toByteString (args !! 0))
    
    -- Load repository
    r <- loadRepositoryFromCurrentDir
    case r of
        Nothing -> do
            putStrLn "fatal: not a git repository"
            exitFailure
        Just repo -> do
            -- Print revisions
            walkHistory repo (V.singleton hash) printCommit ()
