{-# LANGUAGE OverloadedStrings #-}

module GitObject (
    GitHash,
    GitObject (..),
    GitTree,
    GitTreeItem(..),
    loadLooseObject,
    parseCommit,
    parseTree
) where

import qualified Codec.Compression.Zlib as Zlib
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.Vector as V

import Control.Monad
import System.Directory
import System.IO.Posix.MMap.Lazy

import GitUtil

type GitHash = BS.ByteString

data GitTreeItem = GitTreeItem {
    treeItemMode :: BS.ByteString,
    treeItemName :: BS.ByteString,
    treeItemId   :: GitHash
} deriving (Show)

type GitTree = V.Vector GitTreeItem

data GitObject =
    GitBlob B.ByteString |
    GitTree GitTree |
    GitCommit {
        commitTreeId    :: GitHash,
        commitParentIds :: V.Vector GitHash,
        commitAuthor    :: BS.ByteString,
        commitCommitter :: BS.ByteString,
        commitMessage   :: BS.ByteString
    }
    deriving (Show)

parseTree bs = GitTree (V.unfoldr step bs) where
    step bs = if B.null bs then Nothing else Just (treeItem, rest) where
        (mode,bs')      = splitAtChar 32 bs
        (filename,bs'') = splitAtChar 0 bs'
        hash            = B.take 20 bs''
        rest            = B.drop 20 bs''
        treeItem        = GitTreeItem (toStrict mode) (toStrict filename) (toStrict hash)

parseCommit bs = case (parseCommitMaybe bs) of
    Just (treeId, parentIds, author, committer, message) ->
               GitCommit treeId parentIds author committer message
    Nothing -> error "Cannot parse commit"

parseCommitMaybe bs = do
    (treeId, bs1)   <- parseLine "tree " bs
    (parentIds,bs2) <- parseLines "parent " bs1
    (author,bs3)    <- parseLine "author " bs2
    (committer,bs4) <- parseLine "committer " bs3
    let message      = toStrict bs4
    return ((fromHex treeId), (V.map fromHex parentIds), author, committer, message)
    
    where
        parseLine prefix buf =
            let l = B.length prefix
                p = B.take l buf
                (line,rest) = splitAtChar 10 (B.drop l buf)
                line' = toStrict line
            in
                if p == prefix then Just (line',rest) else Nothing

        parseLines prefix buf = parseLines' prefix buf V.empty
        parseLines' prefix buf v = case (parseLine prefix buf) of
            Just (line,rest) -> parseLines' prefix rest (V.snoc v line)
            Nothing -> Just (v,buf)

loadLooseObject filename = do
    contents <- liftM Zlib.decompress (unsafeMMapFile filename)
    let (objHeader, objBody) = splitAtChar 0 contents
        (objType, objLenStr) = splitAtChar 32 objHeader
    
    case objType of
        "blob"   -> return $ GitBlob objBody
        "tree"   -> return $ parseTree objBody
        "commit" -> return $ parseCommit objBody
        _        -> error "Unknown object"
