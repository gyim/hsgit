{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}

module GitPack (
    GitPack (..),
    loadPack,
    readPackObject
) where

import qualified Codec.Compression.Zlib as Zlib
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as VU

import Control.Monad
import Data.Binary
import Data.Binary.Get
import Data.Bits
import Data.Int
import Debug.Trace
import System.Directory
import System.FilePath
import System.IO.Posix.MMap.Lazy

import GitObject
import GitUtil

---- Basic pack types ----
data GitPackIndex = GitPackIndex {
    indexFanoutTable :: VU.Vector Int32,
    indexHashTable :: B.ByteString,
    indexOffsetTable :: B.ByteString
} deriving (Show)

type GitPackData = B.ByteString

data GitPack = GitPack {
	packIndex :: GitPackIndex,
	packData :: GitPackData
}

---- Pack entry ----
data GitPackWholeEntry =
    GitPackBlobEntry B.ByteString |
    GitPackTreeEntry B.ByteString |
    GitPackCommitEntry B.ByteString |
    GitPackTagEntry B.ByteString

data GitPackDeltaEntry =
    GitPackRefDeltaEntry GitHash [GitDeltaCommand] |
    GitPackOfsDeltaEntry Int32 [GitDeltaCommand]

data GitDeltaCommand =
    GitDeltaCopy Int32 Int32 |
    GitDeltaPaste B.ByteString

type GitPackEntry = Either GitPackWholeEntry GitPackDeltaEntry

---- Index file parser ----
getPackIndex :: Get GitPackIndex
getPackIndex = do
    -- TODO:
    -- 1) support v1 index files
    -- 2) support packfiles larger than 2GB
    
    let getNum32 = liftM fromIntegral getWord32be
    
    -- Read header
    magicNum <- getWord32be
    version <- getWord32be
    when (magicNum /= 0xff744f63 || version /= 2) $ fail "Invalid pack header"
    
    -- Read fanout table
    fanoutTable <- VU.replicateM 256 getNum32
    let numObjects = fromIntegral $ VU.last fanoutTable
    
    -- Hash & offset table
    hashTable   <- getLazyByteString (20 * numObjects)
    crcTable    <- getLazyByteString (4 * numObjects)
    offsetTable <- getLazyByteString (4 * numObjects)
    
    return $ GitPackIndex fanoutTable hashTable offsetTable

packEntryNumber :: GitPackIndex -> GitHash -> Maybe Int
packEntryNumber index hash =
    let fanoutTable = indexFanoutTable index
        firstByte = fromIntegral $ BS.head hash
        firstIndex = fromIntegral $ if firstByte == 0 then 0 else (fanoutTable VU.! (firstByte-1))
        lastIndex = fromIntegral $ fanoutTable VU.! firstByte
        hashTable = indexHashTable index
        lazyHash = toLazy hash
        getHashByIndex i = (B.take 20) . (B.drop ((fromIntegral i)*20)) $ hashTable
    in
        if lastIndex > firstIndex
            then binarySearch getHashByIndex lazyHash firstIndex (lastIndex-1)
            else Nothing

objOffset :: GitPackIndex -> GitHash -> Maybe Int32
objOffset index hash = do    
    let offsetTable = indexOffsetTable index
        elementAtIndex i = (B.take 4) . (B.drop ((fromIntegral i)*4)) $ offsetTable
        getOffsetByIndex = fromIntegral . (runGet getWord32be) . elementAtIndex
    hashIndex <- packEntryNumber index hash
    return (getOffsetByIndex hashIndex)

binarySearch :: Ord a => (Int -> a) -> a -> Int -> Int -> Maybe Int
binarySearch getItem a firstIndex lastIndex
    | firstIndex > lastIndex = Nothing
    | cmpResult == GT = binarySearch getItem a firstIndex (currentIndex-1)
    | cmpResult == LT = binarySearch getItem a (currentIndex+1) lastIndex
    | cmpResult == EQ = (Just currentIndex)
    where
        currentIndex = (firstIndex+lastIndex) `div` 2
        cmpResult = compare (getItem currentIndex) a

loadIndex :: FilePath -> IO GitPackIndex
loadIndex path = do
    indexContents <- unsafeMMapFile path
    return $ runGet getPackIndex indexContents

---- Pack file parser ----
getPackEntry :: Get GitPackEntry
getPackEntry = do
    typeByte <- getWord8
    skipEntrySize typeByte
    
    let entryType = (typeByte `shiftR` 4) .&. 0x07
    case entryType of
        1 -> do -- Commit
            entryData <- getDecompressedData
            return $ Left (GitPackCommitEntry entryData)
        2 -> do -- Tree
            entryData <- getDecompressedData
            return $ Left (GitPackTreeEntry entryData)
        3 -> do -- Blob
            entryData <- getDecompressedData
            return $ Left (GitPackBlobEntry entryData)
        4 -> do -- Tag
            entryData <- getDecompressedData
            return $ Left (GitPackTagEntry entryData)
        6 -> do -- OFS delta
            offset <- getDeltaOffset
            entryData <- getDecompressedData
            let delta = runGet getDelta entryData
            return $ Right (GitPackOfsDeltaEntry offset delta)
        7 -> do -- REF delta
            hash <- getByteString 20
            entryData <- getDecompressedData
            let delta = runGet getDelta entryData
            return $ Right (GitPackRefDeltaEntry hash delta)
        _ -> fail $ "Unknown pack entry type: " ++ (show entryType)
    where
        skipEntrySize :: Word8 -> Get ()
        skipEntrySize lastByte =
            if isMSB (fromIntegral lastByte)
                then getWord8 >>= \b -> skipEntrySize b
                else return ()
        
        getDecompressedData :: Get B.ByteString
        getDecompressedData = liftM Zlib.decompress getRemainingLazyByteString
        
        getDeltaOffset :: Get Int32
        getDeltaOffset = do
            firstByte <- liftM fromIntegral getWord8
            if isMSB firstByte
                then getDeltaOffset' (firstByte .&. 0x7f)
                else return firstByte
            where
                getDeltaOffset' n = do
                    b <- liftM fromIntegral getWord8
                    let n' = ((n+1) `shiftL` 7) .|. (b .&. 0x7f)
                    if isMSB b then (getDeltaOffset' n') else (return n')
        
        getDelta :: Get [GitDeltaCommand]
        getDelta = let
            getDeltaL l = do
                cmd <- liftM fromIntegral getWord8
                delta <- if isMSB cmd
                    then do
                        -- Copy command
                        copyOffset <- liftM fromIntegral $ getDeltaCopyLength (cmd .&. 0x0f)
                        copySize <- liftM fromIntegral $ getDeltaCopyLength ((cmd .&. 0x70) `shiftR` 4)
                        
                        return $ GitDeltaCopy copyOffset copySize
                    else do
                        -- Paste command
                        let pasteSize = cmd
                        pasteData <- getLazyByteString (fromIntegral pasteSize)
                        return $ GitDeltaPaste pasteData
                
                empty <- isEmpty
                if empty then return (delta:l) else getDeltaL (delta:l)
            in do
                skipEntrySize 0xff -- original size
                skipEntrySize 0xff -- result size
                liftM reverse (getDeltaL [])
        
        getDeltaCopyLength :: Int32 -> Get Int32
        getDeltaCopyLength mask = getDeltaCopyLength' mask shifts 0 where
            shifts = [(1, 0), (2, 8), (4, 16), (8, 24)]
            getDeltaCopyLength' mask [] acc = return acc
            getDeltaCopyLength' mask ((m,s):ss) acc
                | (mask .&. m) > 0 = do
                    b <- liftM fromIntegral getWord8
                    let acc' = acc .|. (b `shiftL` s)
                    getDeltaCopyLength' mask ss acc'
                | otherwise = getDeltaCopyLength' mask ss acc
        
        isMSB :: Int32 -> Bool
        isMSB n = (n .&. 0x80) > 0

applyDelta :: B.ByteString -> [GitDeltaCommand] -> B.ByteString
applyDelta buf cmds = B.concat chunks where
    chunks = map applyDeltaCmd cmds
    applyDeltaCmd (GitDeltaCopy offset length) = B.take len (B.drop off buf) where
        len = fromIntegral length
        off = fromIntegral offset
    applyDeltaCmd (GitDeltaPaste b) = b

readPackEntryByOffset :: GitPack -> Int32 -> Maybe GitPackWholeEntry
readPackEntryByOffset pack offset = do
    let objData = B.drop (fromIntegral offset) (packData pack)
    let entry = runGet getPackEntry objData
    
    case entry of
        Left e -> return e
        Right (GitPackOfsDeltaEntry offset' delta) -> do
            origEntry <- readPackEntryByOffset pack (offset-offset')
            return $ applyEntry origEntry delta
        Right (GitPackRefDeltaEntry hash delta) -> do
            origEntry <- readPackEntryByHash pack hash
            return $ applyEntry origEntry delta
        where
            applyEntry origEntry delta = case origEntry of
                GitPackBlobEntry b -> GitPackBlobEntry (applyDelta b delta)
                GitPackTreeEntry b -> GitPackTreeEntry (applyDelta b delta)
                GitPackCommitEntry b -> GitPackCommitEntry (applyDelta b delta)
                GitPackTagEntry b -> GitPackTagEntry (applyDelta b delta)

readPackEntryByHash :: GitPack -> GitHash -> Maybe GitPackWholeEntry
readPackEntryByHash pack hash = do
    hashOffset <- objOffset (packIndex pack) hash
    readPackEntryByOffset pack hashOffset

---- Public functions ----
loadPack :: FilePath -> IO GitPack
loadPack path = do
	index <- loadIndex $ (take ((length path)-5) path) ++ ".idx"
	contents <- unsafeMMapFile path
	return $ GitPack index contents

readPackObject :: GitPack -> GitHash -> Maybe GitObject
readPackObject pack hash = do
    entry <- readPackEntryByHash pack hash
    case entry of
        GitPackBlobEntry b -> return (GitBlob b)
        GitPackTreeEntry b -> return (parseTree b)
        GitPackCommitEntry b -> return (parseCommit b)
        GitPackTagEntry b -> fail "Tag is not supported yet."
