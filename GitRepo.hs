{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}

module GitRepo (
    GitRepository (..),
    findAndLoadRepository,
    loadIndex,
    loadRepository,
    loadRepositoryFromCurrentDir,
    loadObject,
    walkHistory
) where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Data.Vector as V
import qualified Data.Set as S

import Control.Monad
import Control.Monad.State
import Data.List
import Debug.Trace
import System.Directory
import System.FilePath

import GitIndex
import GitObject
import GitPack
import GitUtil

data GitRepository = GitRepository {
    repoPath :: String,
    repoPacks :: V.Vector GitPack
}

isGitRepository :: FilePath -> IO Bool
isGitRepository path =
    let gitDir = path </> ".git"
        gitDirExists = doesDirectoryExist gitDir
        headExists = doesFileExist (gitDir </> "HEAD")
        objectsDirExists = doesDirectoryExist (gitDir </> "objects")
        packDirExists = doesDirectoryExist (gitDir </> "objects" </> "pack")
        refsExists = doesDirectoryExist (gitDir </> "refs")
    in
        gitDirExists `andM` headExists `andM` objectsDirExists `andM` packDirExists `andM` refsExists

loadRepository :: FilePath -> IO GitRepository
loadRepository path = do
    packDirContents <- liftM V.fromList $ getDirectoryContents packDir
    packFiles <- V.mapM canonicalizePath (filterAndAbsolute packDirContents)
    packs <- V.forM packFiles loadPack
    return $ GitRepository path packs

    where
        packDir = path </> ".git" </> "objects" </> "pack"
        filterAndAbsolute = (V.map (packDir </>)) . (V.filter isPackFile)
        isPackFile = isSuffixOf ".pack"

findAndLoadRepository :: FilePath -> IO (Maybe GitRepository)
findAndLoadRepository path = do
    isRepo <- isGitRepository path
    if isRepo
        then liftM Just $ loadRepository path
        else do
            parentDir <- canonicalizePath (path </> "..")
            if parentDir == path
                then return Nothing
                else findAndLoadRepository parentDir

loadRepositoryFromCurrentDir :: IO (Maybe GitRepository)
loadRepositoryFromCurrentDir = getCurrentDirectory >>= findAndLoadRepository

loadObject :: GitRepository -> GitHash -> IO GitObject
loadObject repo hash = do
    let looseFilename = (repoPath repo) </> ".git" </> "objects" </> h1 </> h2
        h1 = take 2 hashS
        h2 = drop 2 hashS
        hashS = toString (toHex hash)
    
    obj <- vecFindFirstM (repoPacks repo) (\pack -> return $ readPackObject pack hash)
    case obj of
        Just o  -> return o
        Nothing -> loadLooseObject looseFilename

type HistoryWalkState a = (V.Vector GitHash, S.Set GitHash, a)
type HistoryWalkFunc a = GitHash -> GitObject -> a -> IO (Maybe a)

walkHistory :: GitRepository -> V.Vector GitHash -> HistoryWalkFunc a -> a -> IO (HistoryWalkState a)
walkHistory repo heads walker walkerState = execStateT walkHistory' initialState where
    initialState = (heads, S.empty, walkerState)
    
    walkHistory' = do
        (commitIds,visitedCommits,walkerState) <- get
        let commitId = V.head commitIds
            commitListNotEmpty = not (V.null commitIds)
            shouldVisitCommit = not (S.member commitId visitedCommits)
        
        when commitListNotEmpty $ do
            if shouldVisitCommit
                then do
                    commit <- liftIO $ loadObject repo commitId
                    walkerState' <- liftIO $ walker commitId commit walkerState

                    let visitedCommits' = S.insert commitId visitedCommits
                        commitIds' = (V.drop 1 commitIds) V.++ (commitParentIds commit)
                    
                    case walkerState' of
                        Just s -> do
                            put (commitIds',visitedCommits', s)
                            walkHistory'
                        Nothing -> return ()
                else do
                    let commitIds' = (V.drop 1 commitIds)
                    put (commitIds',visitedCommits,walkerState)
                    walkHistory'

loadIndex :: GitRepository -> IO GitIndex
loadIndex repo = do
    let filename = (repoPath repo) </> ".git" </> "index"
    loadIndexFile filename
