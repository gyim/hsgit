GHC=ghc -O2 -fvia-C

ifeq ($(PROFILING),1)
	GHC += -prof -auto-all -caf-all
endif

SRCS = \
	GitObject.hs \
	GitPack.hs \
	GitRepo.hs \
	GitUtil.hs \
	GitIndex.hs

PROGS = \
	git-show \
	git-rev-list \
	git-ls-cache

all: $(PROGS)

clean:
	rm -f $(PROGS) *.o *.hi

$(PROGS): $(SRCS)
	$(GHC) --make $@.hs
