import qualified Data.Vector as V

import Control.Monad
import System.Environment
import System.Exit

import GitIndex
import GitObject
import GitRepo
import GitUtil

main = do
    -- Load repository
    r <- loadRepositoryFromCurrentDir
    case r of
        Nothing -> do
            putStrLn "fatal: not a git repository"
            exitFailure
        Just repo -> do
            -- Print object
            index <- loadIndex repo
            V.forM (indexEntries index) (\entry -> do
                putStrLn $ (show (indexEntryName entry)) ++ " size=" ++ (show (indexEntrySize entry))
                )
